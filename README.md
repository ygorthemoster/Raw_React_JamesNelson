# Codementor: Raw React — no JSX, no Flux, no ES6, no Webpack
This is an implementation of a tutorial by James K Nelson, the tutorial is freely avaliable at: http://jamesknelson.com/learn-raw-react-no-jsx-flux-es6-webpack/, the article describes the basics of React.js and guides you through creating a simple component, this code is the final product of said tutorial

## Pre-requisites
To test this project you'll need
- [git](https://git-scm.com/)

## Installing

just clone this repository by using this command

```
git clone https://ygorthemoster@gitlab.com/ygorthemoster/Raw_React_JamesNelson.git
```

## Running
open the [src.html](src.html) file in a browser

## License
See [LICENSE](LICENSE)

## Acknowledgments
- Original source and tutorial by [James K Nelson](http://jamesknelson.com) available at: http://jamesknelson.com/learn-raw-react-no-jsx-flux-es6-webpack/
